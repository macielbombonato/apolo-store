package br.apolo.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import br.apolo.data.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>, ProductRepositoryCustom {

	Page<Product> findByQtdAvailableGreaterThanOrderByNameAsc(@Param("qtdAvailable") Integer qtdAvailable, Pageable page);
	
	Page<Product> findByNameLikeOrderByNameAsc(@Param("name") String name, Pageable page);
	
}
