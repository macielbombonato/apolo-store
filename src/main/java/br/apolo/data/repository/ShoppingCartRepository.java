package br.apolo.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.apolo.data.model.ShoppingCart;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long>, ShoppingCartRepositoryCustom {

}
