package br.apolo.data.repository;

import br.apolo.data.model.Product;

public interface ProductRepositoryCustom extends BaseRepository<Product> {

}
