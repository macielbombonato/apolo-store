package br.apolo.data.repository;

import br.apolo.data.model.ShoppingCart;

public interface ShoppingCartRepositoryCustom extends BaseRepository<ShoppingCart> {

}
