package br.apolo.data.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import br.apolo.common.util.InputLength;
import br.apolo.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "product")
@AttributeOverride(name = "id", column = @Column(name = "product_id"))
public class Product extends AuditableBaseEntity {

	private static final long serialVersionUID = -4203024606423438380L;

	@Column(name = "name", length = InputLength.NAME, nullable = false)
	@NotNull
	@Size(min = 1, max = InputLength.NAME)
	private String name;
	
	@Column(name = "description", length = InputLength.DESCR, nullable = true)
	@Size(max = InputLength.DESCR)
	private String description;
	
	@Column(name = "qtd_available", nullable = false)
	@NotNull
	private Integer qtdAvailable;
	
	@Column(name = "price", nullable = false)
	@NotNull
	private BigDecimal price;

	@Column(name = "pic_original_name", length = InputLength.MEDIUM, nullable = true)
	private String pictureOriginalName;

	@Column(name = "pic_generated_name", length = InputLength.MEDIUM, nullable = true)
	private String pictureGeneratedName;
	
	@Transient
	private List<MultipartFile> picturefiles;
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getPictureOriginalName() {
		return pictureOriginalName;
	}

	public void setPictureOriginalName(String pictureOriginalName) {
		this.pictureOriginalName = pictureOriginalName;
	}

	public String getPictureGeneratedName() {
		return pictureGeneratedName;
	}

	public void setPictureGeneratedName(String pictureGeneratedName) {
		this.pictureGeneratedName = pictureGeneratedName;
	}

	public List<MultipartFile> getPicturefiles() {
		return picturefiles;
	}

	public void setPicturefiles(List<MultipartFile> picturefiles) {
		this.picturefiles = picturefiles;
	}

	public Integer getQtdAvailable() {
		return this.qtdAvailable;
	}

	public void setQtdAvailable(Integer qtdAvailable) {
		this.qtdAvailable = qtdAvailable;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
