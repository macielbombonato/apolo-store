package br.apolo.data.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.apolo.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "product_in_cart")
@AttributeOverride(name = "id", column = @Column(name = "product_in_cart_id"))
public class ProductInCart extends AuditableBaseEntity {

	private static final long serialVersionUID = 5105139448180111289L;
	
	@ManyToOne
	private ShoppingCart shoppingCart;
	
	@ManyToOne
	private Product product;
	
	@Column(name = "qtd_selected", nullable = false)
	@NotNull
	private Integer qtdSelected;

	public ShoppingCart getShoppingCart() {
		return this.shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQtdSelected() {
		return this.qtdSelected;
	}

	public void setQtdSelected(Integer qtdSelected) {
		this.qtdSelected = qtdSelected;
	}

}
