package br.apolo.data.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.apolo.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "shopping_cart")
@AttributeOverride(name = "id", column = @Column(name = "shopping_cart_id"))
public class ShoppingCart extends AuditableBaseEntity {

	private static final long serialVersionUID = 5105139448180111289L;
	
	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "shoppingCart", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ProductInCart> products;

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ProductInCart> getProducts() {
		return this.products;
	}

	public void setProducts(List<ProductInCart> products) {
		this.products = products;
	}

}
