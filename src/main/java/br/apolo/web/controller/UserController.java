package br.apolo.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.apolo.business.model.FileContent;
import br.apolo.business.service.FileService;
import br.apolo.business.service.UserCustomFieldService;
import br.apolo.business.service.UserGroupService;
import br.apolo.business.service.UserService;
import br.apolo.common.util.MessageBundle;
import br.apolo.data.enums.Status;
import br.apolo.data.enums.UserPermission;
import br.apolo.data.model.User;
import br.apolo.security.SecuredEnum;
import br.apolo.web.enums.Navigation;

@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController<User> {

	private final String ACCEPTED_FILE_TYPE = ".gif.jpg.png";
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserGroupService userGroupService;
	
	@Autowired
	UserCustomFieldService userCustomFieldService;
	
	@Autowired
	private FileService<User> fileService;
	
	@Autowired
	AppController appController;
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_INDEX, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_INDEX.getPath());
		
		mav.addObject("user", userService.getAuthenticatedUser());
		mav.addObject("readOnly", true);
		return mav;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "change-password", method = RequestMethod.GET)
	public ModelAndView changePassword(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_CHANGE_PASSWORD, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_CHANGE_PASSWORD.getPath());
		
		mav.addObject("user", userService.getAuthenticatedUser());
		mav.addObject("readOnly", true);
		mav.addObject("changePassword", true);
		return mav;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "change-password-save", method = RequestMethod.POST)
	public ModelAndView changePasswordSave(@ModelAttribute("user") User user, HttpServletRequest request, @RequestParam(defaultValue = "") String passwordConfirmation) {
		ModelAndView mav = index(request);
		
		if (entityHasErrors(user, true, passwordConfirmation)) {
			mav.setViewName(getRedirectionPath(request, Navigation.USER_CHANGE_PASSWORD, Navigation.USER_CHANGE_PASSWORD));
			mav.addObject("user", userService.getAuthenticatedUser());
			mav.addObject("readOnly", true);
			mav.addObject("changePassword", true);
			mav.addObject("error", true);
			
			StringBuilder message = new StringBuilder();
			message.append(additionalValidation(user, true, passwordConfirmation));
			
			mav.addObject("message", message.toString());
			
			return mav;
		} 
		
		if (user != null) {
			User dbuser = userService.find(user.getId());
			
			dbuser.setPassword(user.getPassword());
			
			userService.save(dbuser, true, null);
		}
		
		return mav;
	}

	@SecuredEnum(UserPermission.USER_CREATE)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_NEW, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_NEW.getPath());
		
		User user = new User();
		
		user.setStatus(Status.ACTIVE);
		user.setCreatedBy(userService.getAuthenticatedUser());
		user.setCreationDate(new Date());
		
		user.setLastUpdatedBy(userService.getAuthenticatedUser());
		user.setLastUpdateDate(new Date());
		
		mav.addObject("user", user);
		mav.addObject("groupList", userGroupService.list());
		mav.addObject("customFieldList", userCustomFieldService.list());
		mav.addObject("readOnly", false);
		
		return mav;
	}
	
	@RequestMapping(value = "signin", method = RequestMethod.GET)
	public ModelAndView signin(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_SIGNIN, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_SIGNIN.getPath());
		
		User user = new User();
		
		user.setStatus(Status.ACTIVE);
		
		mav.addObject("user", user);
		mav.addObject("customFieldList", userCustomFieldService.list());
		mav.addObject("readOnly", false);
		
		return mav;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_EDIT, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_EDIT.getPath());
		
		// The authenticated user can change your own profile or others if has user edit permission
		if (authenticatedUserHasPermission(userService, id, UserPermission.USER_EDIT)) {
			User user = userService.find(id);
			
			user.setLastUpdatedBy(userService.getAuthenticatedUser());
			user.setLastUpdateDate(new Date());
			
			mav.addObject("user", user);
			mav.addObject("groupList", userGroupService.list());
			mav.addObject("customFieldList", userCustomFieldService.list());
			mav.addObject("readOnly", false);
			mav.addObject("editing", true);			
		} else {
			String message = MessageBundle.getMessageBundle("user.msg.error.permission");
			throw new AccessDeniedException(message);
		}
		
		return mav;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_VIEW, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_VIEW.getPath());
		
		if (authenticatedUserHasPermission(userService, id, UserPermission.USER_LIST)) {
			User user = userService.find(id);
			
			mav.addObject("user", user);
			mav.addObject("customFieldList", userCustomFieldService.list());
			mav.addObject("readOnly", true);			
		} else {
			String message = MessageBundle.getMessageBundle("user.msg.error.permission");
			throw new AccessDeniedException(message);
		}
		
		return mav;
	}
	
	@Override
	@SecuredEnum(UserPermission.USER_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public @ResponseBody String remove(@PathVariable Long id) {
		String result = "";
		
		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();
		
		User user = userService.find(id);
		
		if (user != null) {
			if (!userService.getAuthenticatedUser().equals(user)) {
				try {
					userService.remove(user);
					
					result = MessageBundle.getMessageBundle("common.msg.remove.success");
					jsonItem.put("success", true);
				} catch (Throwable e) {
					result = MessageBundle.getMessageBundle("common.remove.msg.error");
					jsonItem.put("success", false);
				}
			} else {
				result = MessageBundle.getMessageBundle("user.msg.error.remove.yourself");
				jsonItem.put("success", false);
			}
		}
		
		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);
		
		return jsonSubject.toString();
	}
	
	@SecuredEnum(UserPermission.USER_REMOVE)
	@RequestMapping(value = "remove-registry/{id}", method = RequestMethod.GET)
	public ModelAndView removeRegistry(@PathVariable Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView(Navigation.USER_LIST.getPath());
		
		User user = userService.find(id);
		
		if (user != null) {
			try {
				userService.remove(user);

				mav = list(request);
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.success"));
			} catch (Throwable e) {
				mav = list(request);
				mav.addObject("error", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.remove.msg.error"));
			}
		}
		
		return mav;
	}
	
	
	/**
	 * Use public ModelAndView save(@ModelAttribute("user") User user, @RequestParam(defaultValue = "false") boolean changePassword)
	 */
	@Override
	public ModelAndView save(User entity, BindingResult result, HttpServletRequest request) {
		return null;
	}
	
	@RequestMapping(value = "save-signin", method = RequestMethod.POST)
	public String saveSignin(@Valid @ModelAttribute("user") User entity, BindingResult result, HttpServletRequest request, @RequestParam(defaultValue = "false") boolean changePassword, @RequestParam(defaultValue = "") String passwordConfirmation) {
		
		save(entity, result, request, changePassword, passwordConfirmation);
		
		return appController.index(null, request);
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("user") User entity, BindingResult result, HttpServletRequest request, @RequestParam(defaultValue = "false") boolean changePassword, @RequestParam(defaultValue = "") String passwordConfirmation) {
		ModelAndView mav = new ModelAndView();
		
		MultipartFile objectFile = null;
		
		List<MultipartFile> files = null;
		
		if (entity.getPicturefiles() != null) {
			files = entity.getPicturefiles();
		}
		
		if(files != null && !files.isEmpty()) {
			for (MultipartFile multipartFile : files) {
				objectFile = multipartFile;
			}
		}
		
		if (objectFile != null 
				&& objectFile.getOriginalFilename() != null 
				&& !objectFile.getOriginalFilename().isEmpty()) {
			entity.setPictureOriginalName(objectFile.getOriginalFilename());
			entity.setPictureGeneratedName(objectFile.getOriginalFilename());
		}
		
		/*
		 * Object validation
		 */
		if (result.hasErrors() || entityHasErrors(entity, changePassword, passwordConfirmation)) {
			mav.setViewName(getRedirectionPath(request, Navigation.USER_NEW, Navigation.USER_EDIT));
			mav.addObject("user", entity);
			mav.addObject("groupList", userGroupService.list());
			mav.addObject("customFieldList", userCustomFieldService.list());
			mav.addObject("readOnly", false);
			mav.addObject("error", true);
			
			/*
			 * especific validation to show or not the password field
			 */
			String referer = request.getHeader("referer");
			if (referer != null && referer.contains(Navigation.USER_EDIT.getPath())) {
				mav.addObject("editing", true);
			}
			
			StringBuilder message = new StringBuilder();
			for (ObjectError error : result.getAllErrors()) {
				DefaultMessageSourceResolvable argument = (DefaultMessageSourceResolvable) error.getArguments()[0];
				
				message.append(MessageBundle.getMessageBundle("common.field") + " " + MessageBundle.getMessageBundle("user." + argument.getDefaultMessage()) + ": " + error.getDefaultMessage() + "\n <br />");
			}
			
			message.append(additionalValidation(entity, changePassword, passwordConfirmation));
			
			mav.addObject("message", message.toString());
			
			return mav;
		} 
		
		if (entity != null) {
			if (authenticatedUserHasPermission(userService, entity.getId(), UserPermission.USER_CREATE) 
					|| authenticatedUserHasPermission(userService, entity.getId(), UserPermission.USER_EDIT)) {
				FileContent file = null;
				
				if (objectFile != null) {
					file = new FileContent();
					file.setFile(objectFile);
				}
				
				userService.save(entity, changePassword, file);
				
				mav = view(entity.getId(), request);
				
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));				
			} else {
				String message = MessageBundle.getMessageBundle("user.msg.error.permission");
				throw new AccessDeniedException(message);
			}
		}
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.USER_MANAGER)
	@RequestMapping(value = "lock/{id}", method = RequestMethod.GET)
	public ModelAndView lock(@PathVariable Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView(Navigation.USER_EDIT.getPath());
		
		User user = userService.find(id);
		
		if (user != null) {
			
			userService.lock(user);
			
			mav = view(user.getId(), request);
			
			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
		}
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.USER_MANAGER)
	@RequestMapping(value = "unlock/{id}", method = RequestMethod.GET)
	public ModelAndView unlock(@PathVariable Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView(Navigation.USER_EDIT.getPath());
		
		User user = userService.find(id);
		
		if (user != null) {
			
			userService.unlock(user);
			
			mav = view(user.getId(), request);
			
			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
		}
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.USER_MANAGER)
	@RequestMapping(value = "list-locked", method = RequestMethod.GET)
	public ModelAndView listLocked(HttpServletRequest request) {
		return listLocked(1, request);
	}
	
	@SecuredEnum(UserPermission.USER_MANAGER)
	@RequestMapping(value = "list-locked/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView listLocked(@PathVariable Integer pageNumber, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_LIST.getPath());
		
		Page<User> page = userService.listLocked(pageNumber);
		
		configurePageable(mav, page, "/user/list-locked");
		
		mav.addObject("searchParameter", "");
		
		if (page != null && page.getContent() != null) {
			mav.addObject("userList", page.getContent());	
		}
		
		return mav;
	}

	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request) {
		return list(1, request);
	}
	
	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "list/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView list(@PathVariable Integer pageNumber, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_LIST.getPath());
		
		Page<User> page = userService.list(pageNumber);
		
		configurePageable(mav, page, "/user/list");
		
		mav.addObject("searchParameter", "");
		
		if (page != null && page.getContent() != null) {
			mav.addObject("userList", page.getContent());	
		}
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute("searchParameter") String searchParameter, HttpServletRequest request) {
		return search(1, searchParameter, request);
	}
	
	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "search/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView search(@PathVariable Integer pageNumber, HttpServletRequest request) {
		return search(pageNumber, "", request);
	}
	
	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "search/{searchParameter}/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView search(@PathVariable Integer pageNumber, @PathVariable String searchParameter, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_LIST.getPath());
		
		Page<User> page = userService.search(pageNumber, searchParameter);
		
		String url = "";
		
		if (searchParameter == null || "".equalsIgnoreCase(searchParameter)) {
			url = "/user/search";
		} else {
			url = "/user/search/"+searchParameter;
		}
		
		configurePageable(mav, page, url);
		
		mav.addObject("searchParameter", searchParameter);
		
		if (page != null && page.getContent() != null) {
			mav.addObject("userList", page.getContent());	
		}
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.USER_LIST)
	@RequestMapping(value = "search-form", method = RequestMethod.GET)
	public ModelAndView searchForm(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.USER_SEARCH, request);
		
		ModelAndView mav = new ModelAndView(Navigation.USER_SEARCH.getPath());
		
		return mav;
	}
	
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Set.class, "groups", new CustomCollectionEditor(Set.class) {
            @Override
            protected Object convertElement(Object element) {
                Long id = null;

                if(element instanceof String && !((String)element).equals("")){
                    //From the JSP 'element' will be a String
                    try{
                        id = Long.parseLong((String) element);
                    } catch (NumberFormatException e) {
                        log.error("Element was " + ((String) element), e);
                    }
                } else if(element instanceof Long) {
                    //From the database 'element' will be a Long
                    id = (Long) element;
                }

                return id != null ? userGroupService.find(id) : null;
            }
          });
    }
    
    private boolean entityHasErrors(User entity, boolean changePassword, String passwordConfirmation) {
		boolean hasErrors = false;
		
		if (entity != null) {
			if (validateEmail(entity)) {
				hasErrors = true;
			} else if(changePassword && !entity.getPassword().equals(passwordConfirmation)) {
				hasErrors = true;
			} else if (isValidFileType(entity)) {
				hasErrors = true;
			}
		}
		
		return hasErrors;
	}
    
    private boolean validateEmail(User entity){
    	boolean hasError = false;
    	
    	User result = userService.findByLogin(entity.getEmail());
		
		if(result != null 
				&& !result.getId().equals(entity.getId())){
			hasError = true;
		}
		
		return hasError;
    }
    
    private String additionalValidation(User entity, boolean changePassword, String passwordConfirmation) {
		StringBuilder message = new StringBuilder();
		
		if (entity != null) {
			if (validateEmail(entity)) {
				message.append(MessageBundle.getMessageBundle("user.email") + ": " + MessageBundle.getMessageBundle("user.email.duplicate") + "\n <br />");
			}
			
			if(changePassword && !entity.getPassword().equals(passwordConfirmation)) {
				message.append(MessageBundle.getMessageBundle("user.password.confirmation") + ": " + MessageBundle.getMessageBundle("user.password.confirmatin.failure") + "\n <br />");
			}
			
			if (isValidFileType(entity)) {
				message.append(MessageBundle.getMessageBundle("user.picturefiles") + ": " + MessageBundle.getMessageBundle("user.fileType") + "\n <br />");
			}
		}
		
		return message.toString();
	}
    
	private boolean isValidFileType(User entity) {
		boolean hasErrors = false;
		
		if (entity.getPictureOriginalName() == null 
				|| (entity.getPictureOriginalName() != null
					&& entity.getPictureOriginalName().length() > 0
					&& !ACCEPTED_FILE_TYPE.contains(fileService.extractFileExtension(entity.getPictureOriginalName()))
				)) {
			hasErrors = true;
		}
		
		return hasErrors;
	}

}
