package br.apolo.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.apolo.business.model.FileContent;
import br.apolo.business.service.FileService;
import br.apolo.business.service.ProductService;
import br.apolo.business.service.UserService;
import br.apolo.common.util.MessageBundle;
import br.apolo.data.enums.UserPermission;
import br.apolo.data.model.Product;
import br.apolo.data.model.ProductInCart;
import br.apolo.data.model.ShoppingCart;
import br.apolo.security.SecuredEnum;
import br.apolo.web.enums.Navigation;
import br.apolo.web.model.CartViewModel;
import br.apolo.web.model.ProductViewModel;

@Controller
@RequestMapping(value = "/product")
public class ProductController extends BaseController<Product> {

	private final String ACCEPTED_FILE_TYPE = ".gif.jpg.png";
	
	@Autowired
	UserService userService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	private FileService<Product> fileService;
	
	@SecuredEnum(UserPermission.PRODUCT_CREATE)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_NEW, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_NEW.getPath());
		
		Product product = new Product();
		
		product.setCreatedBy(userService.getAuthenticatedUser());
		product.setCreationDate(new Date());
		
		product.setLastUpdatedBy(userService.getAuthenticatedUser());
		product.setLastUpdateDate(new Date());
		
		mav.addObject("product", product);
		mav.addObject("readOnly", false);
		
		return mav;
	}
	
	@SecuredEnum(UserPermission.PRODUCT_EDIT)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_EDIT, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_EDIT.getPath());
		
		Product product = productService.find(id);
		
		product.setLastUpdatedBy(userService.getAuthenticatedUser());
		product.setLastUpdateDate(new Date());
		
		mav.addObject("product", product);
		mav.addObject("readOnly", false);
		
		return mav;
	}

	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_VIEW, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_VIEW.getPath());
		
		Product product = productService.find(id);
		
		mav.addObject("product", product);
		mav.addObject("readOnly", true);
		
		return mav;
	}
	
	@Override
	@SecuredEnum(UserPermission.PRODUCT_REMOVE)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public @ResponseBody String remove(@PathVariable Long id) {
		String result = "";
		
		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();
		
		Product product = productService.find(id);
		
		if (product != null) {
			
			productService.remove(product);
			
			result = MessageBundle.getMessageBundle("common.msg.remove.success");
			jsonItem.put("success", true);
			
		}
		
		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);
		
		return jsonSubject.toString();
	}
	
	@RequestMapping(value = "remove-from-cart/{id}", method = RequestMethod.GET)
	public @ResponseBody String removeProductFromShoppingCart(@PathVariable Long id) {
		String result = "";
		
		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();
		
		ShoppingCart cart = productService.removeProductFromShoppingCart(id);
		
		if (cart != null) {
			CartViewModel cartVM = new CartViewModel();
			cartVM.setTotalPrice(new BigDecimal(0));
			cartVM.setProducts(new ArrayList<ProductViewModel>());
			
			ProductViewModel prodVM = null;
			for (ProductInCart prod : cart.getProducts()) {
				prodVM = new ProductViewModel();
				prodVM.setId(prod.getProduct().getId());
				prodVM.setName(prod.getProduct().getName());
				prodVM.setPrice(prod.getProduct().getPrice());
				prodVM.setQtd(prod.getQtdSelected());
				prodVM.setProductTotalPrice(prodVM.getPrice().multiply(new BigDecimal(prodVM.getQtd())));
				
				cartVM.getProducts().add(prodVM);
				cartVM.setTotalPrice(new BigDecimal(cartVM.getTotalPrice().doubleValue() + prodVM.getProductTotalPrice().doubleValue()));
			}
			
			jsonItem.put("cart", cartVM);
			
			result = MessageBundle.getMessageBundle("common.msg.remove.success");
			jsonItem.put("success", true);
		}
		
		jsonItem.put("message", result);
		jsonSubject.accumulate("result", jsonItem);
		
		return jsonSubject.toString();
	}
	
	@SecuredEnum(UserPermission.PRODUCT_REMOVE)
	@RequestMapping(value = "remove-registry/{id}", method = RequestMethod.GET)
	public ModelAndView removeRegistry(@PathVariable Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_LIST.getPath());
		
		Product product = productService.find(id);
		
		if (product != null) {
			try {
				productService.remove(product);

				mav = list(request);
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.success"));
			} catch (Throwable e) {
				mav = list(request);
				mav.addObject("error", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.remove.msg.error"));
			}
		}
		
		return mav;
	}
	
	
	@PreAuthorize("isAuthenticated()")
	@SecuredEnum({UserPermission.PRODUCT_CREATE, UserPermission.PRODUCT_EDIT})
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("product") Product entity, BindingResult result, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		
		MultipartFile objectFile = null;
		
		List<MultipartFile> files = null;
		
		if (entity.getPicturefiles() != null) {
			files = entity.getPicturefiles();
		}
		
		if(files != null && !files.isEmpty()) {
			for (MultipartFile multipartFile : files) {
				objectFile = multipartFile;
			}
		}
		
		if (objectFile != null 
				&& objectFile.getOriginalFilename() != null 
				&& !objectFile.getOriginalFilename().isEmpty()) {
			entity.setPictureOriginalName(objectFile.getOriginalFilename());
			entity.setPictureGeneratedName(objectFile.getOriginalFilename());
		}
		
		/*
		 * Object validation
		 */
		if (result.hasErrors() || entityHasErrors(entity)) {
			mav.setViewName(getRedirectionPath(request, Navigation.PRODUCT_NEW, Navigation.PRODUCT_EDIT));
			mav.addObject("product", entity);
			mav.addObject("readOnly", false);
			mav.addObject("error", true);
			
			/*
			 * especific validation to show or not the password field
			 */
			String referer = request.getHeader("referer");
			if (referer != null && referer.contains(Navigation.PRODUCT_EDIT.getPath())) {
				mav.addObject("editing", true);
			}
			
			StringBuilder message = new StringBuilder();
			for (ObjectError error : result.getAllErrors()) {
				DefaultMessageSourceResolvable argument = (DefaultMessageSourceResolvable) error.getArguments()[0];
				
				message.append(MessageBundle.getMessageBundle("common.field") + " " + MessageBundle.getMessageBundle("product." + argument.getDefaultMessage()) + ": " + error.getDefaultMessage() + "\n <br />");
			}
			
			message.append(additionalValidation(entity));
			
			mav.addObject("message", message.toString());
			
			return mav;
		} 
		
		if (entity != null) {
			FileContent file = null;
			
			if (objectFile != null) {
				file = new FileContent();
				file.setFile(objectFile);
			}
			
			productService.save(entity, file);
			
			mav = view(entity.getId(), request);
			
			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
		}
		
		return mav;
	}
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request) {
		return list(1, request);
	}
	
	@RequestMapping(value = "list/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView list(@PathVariable Integer pageNumber, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_LIST.getPath());
		
		Page<Product> page = productService.list(pageNumber);
		
		configurePageable(mav, page, "/product/list");
		
		mav.addObject("searchParameter", "");
		
		if (page != null && page.getContent() != null) {
			mav.addObject("productList", page.getContent());	
		}
		
		return mav;
	}
	
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute("searchParameter") String searchParameter, HttpServletRequest request) {
		return search(1, searchParameter, request);
	}
	
	@RequestMapping(value = "search/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView search(@PathVariable Integer pageNumber, HttpServletRequest request) {
		return search(pageNumber, "", request);
	}
	
	@RequestMapping(value = "search/{searchParameter}/{pageNumber}", method = RequestMethod.GET)
	public ModelAndView search(@PathVariable Integer pageNumber, @PathVariable String searchParameter, HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_LIST.getPath());
		
		Page<Product> page = productService.search(pageNumber, searchParameter);
		
		String url = "";
		
		if (searchParameter == null || "".equalsIgnoreCase(searchParameter)) {
			url = "/product/search";
		} else {
			url = "/product/search/"+searchParameter;
		}
		
		configurePageable(mav, page, url);
		
		mav.addObject("searchParameter", searchParameter);
		
		if (page != null && page.getContent() != null) {
			mav.addObject("productList", page.getContent());	
		}
		
		return mav;
	}
	
	@RequestMapping(value = "search-form", method = RequestMethod.GET)
	public ModelAndView searchForm(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_SEARCH, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_SEARCH.getPath());
		
		return mav;
	}
	
    private boolean entityHasErrors(Product entity) {
		boolean hasErrors = false;
		
		if (entity != null) {
			if (isValidFileType(entity)) {
				hasErrors = true;
			}
		}
		
		return hasErrors;
	}
    
    private String additionalValidation(Product entity) {
		StringBuilder message = new StringBuilder();
		
		if (entity != null) {
			if (isValidFileType(entity)) {
				message.append(MessageBundle.getMessageBundle("user.picturefiles") + ": " + MessageBundle.getMessageBundle("user.fileType") + "\n <br />");
			}
		}
		
		return message.toString();
	}
    
	private boolean isValidFileType(Product entity) {
		boolean hasErrors = false;
		
		if (entity.getPictureOriginalName() == null 
				|| (entity.getPictureOriginalName() != null
					&& entity.getPictureOriginalName().length() > 0
					&& !ACCEPTED_FILE_TYPE.contains(fileService.extractFileExtension(entity.getPictureOriginalName()))
				)) {
			hasErrors = true;
		}
		
		return hasErrors;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "addProductToShoppingCart/{hasAdd}/{productId}/{qtd}", method = RequestMethod.GET)
	public @ResponseBody String addProductToShoppingCart(@PathVariable Long productId, @PathVariable Integer qtd, @PathVariable boolean hasAdd) {
		JSONObject jsonSubject = new JSONObject();
		JSONObject jsonItem = new JSONObject();
		
		ShoppingCart cart = productService.addProductToShoppingCart(productId, qtd, hasAdd);
		
		if (cart != null) {
			CartViewModel cartVM = new CartViewModel();
			cartVM.setTotalPrice(new BigDecimal(0));
			cartVM.setProducts(new ArrayList<ProductViewModel>());
			
			ProductViewModel prodVM = null;
			for (ProductInCart prod : cart.getProducts()) {
				prodVM = new ProductViewModel();
				prodVM.setId(prod.getProduct().getId());
				prodVM.setName(prod.getProduct().getName());
				prodVM.setPrice(prod.getProduct().getPrice());
				prodVM.setQtd(prod.getQtdSelected());
				prodVM.setProductTotalPrice(prodVM.getPrice().multiply(new BigDecimal(prodVM.getQtd())));
				
				cartVM.getProducts().add(prodVM);
				cartVM.setTotalPrice(new BigDecimal(cartVM.getTotalPrice().doubleValue() + prodVM.getProductTotalPrice().doubleValue()));
			}
			
			jsonItem.put("cart", cartVM);
			
		}
		
		jsonSubject.accumulate("result", jsonItem);
		
		return jsonSubject.toString();
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "showCart", method = RequestMethod.GET)
	public ModelAndView showCart(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_SHOW_CART, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_SHOW_CART.getPath());
		
		ShoppingCart cart = productService.getShoppingCart();
		
		mav.addObject("shoppingCart", cart);
		
		return mav;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "checkout", method = RequestMethod.GET)
	public ModelAndView checkout(HttpServletRequest request) {
		breadCrumbService.addNode(Navigation.PRODUCT_LIST, request);
		
		ModelAndView mav = new ModelAndView(Navigation.PRODUCT_LIST.getPath());
		
		ShoppingCart cart = productService.checkout();
		
		if (cart != null) {
			mav.addObject("shoppingCart", cart);
		} else {
			mav = list(request);
		}
		
		return mav;
	}

}
