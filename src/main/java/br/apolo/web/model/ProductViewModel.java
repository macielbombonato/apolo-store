package br.apolo.web.model;

import java.math.BigDecimal;

public class ProductViewModel {
	
	private Long id;
	private String name;
	private BigDecimal price;
	private Integer qtd;
	private BigDecimal productTotalPrice;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getPrice() {
		return this.price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getQtd() {
		return this.qtd;
	}
	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}
	public BigDecimal getProductTotalPrice() {
		return this.productTotalPrice;
	}
	public void setProductTotalPrice(BigDecimal productTotalPrice) {
		this.productTotalPrice = productTotalPrice;
	}

}
