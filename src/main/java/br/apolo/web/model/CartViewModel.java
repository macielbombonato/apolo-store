package br.apolo.web.model;

import java.math.BigDecimal;
import java.util.List;

public class CartViewModel {
	
	private List<ProductViewModel> products;
	private BigDecimal totalPrice;
	public List<ProductViewModel> getProducts() {
		return this.products;
	}
	public void setProducts(List<ProductViewModel> products) {
		this.products = products;
	}
	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

}
