package br.apolo.business.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.apolo.business.model.FileContent;
import br.apolo.business.service.FileService;
import br.apolo.business.service.ProductService;
import br.apolo.common.exception.BusinessException;
import br.apolo.common.util.MessageBundle;
import br.apolo.data.model.Product;
import br.apolo.data.model.ProductInCart;
import br.apolo.data.model.ShoppingCart;
import br.apolo.data.repository.ProductRepository;
import br.apolo.data.repository.ShoppingCartRepository;
import br.apolo.security.CurrentUser;

@Service("productService")
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ShoppingCartRepository shoppingCartRepositpry;
	
	@Autowired
	private FileService<Product> fileService;

	@Override
	public List<Product> list() {
		return productRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name")));
	}
	
	@Override
	public Page<Product> list(Integer pageNumber) {
		if (pageNumber < 1) {
			pageNumber = 1;
		}
		
		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "name");
		
		return productRepository.findByQtdAvailableGreaterThanOrderByNameAsc(0, request);
	}

	@Override
	public Product find(Long id) {
		return productRepository.findOne(id);
	}

	@Override
	@Transactional
	public Product save(Product entity) {
		if (entity != null) {
			entity.setLastUpdatedBy(getAuthenticatedUser());
			entity.setLastUpdateDate(new Date());			
		}
		
		return save(entity, null);
	}

	@Override
	@Transactional
	public Product save(Product product, FileContent file) {
		if (file != null) {
			if (file != null 
					&& file.getFile() != null 
					&& file.getFile().getOriginalFilename() != null 
					&& !file.getFile().getOriginalFilename().isEmpty()) {
				
				if (product.getId() == null) {
					productRepository.saveAndFlush(product);	
				}
				
				product.setPictureOriginalName(file.getFile().getOriginalFilename());
				try {
					product.setPictureGeneratedName(fileService.uploadFile(product, file, file.getFile().getInputStream()));
				} catch (IOException e) {
					String message = MessageBundle.getMessageBundle("commons.errorUploadingFile");
					throw new BusinessException(message);
				}
			}
		}
		
		return productRepository.saveAndFlush(product);
	}
	
	@Override
	@Transactional
	public void remove(Product product) {
		productRepository.delete(product);
	}

	@Override
	public Page<Product> search(Integer pageNumber, String param) {
		if (pageNumber < 1) {
			pageNumber = 1;
		}
		
		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "name");
		
		if (param != null) {
			param = "%" + param + "%";	
		}
		
		return productRepository.findByNameLikeOrderByNameAsc(param, request);
	}
	
	@Override
	public ShoppingCart getShoppingCart() {
		ShoppingCart cart = null;
		
		CurrentUser currentUser = null;
		
		try {
			currentUser = (CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch(Throwable errorId) {
			currentUser = null;
			log.error(errorId.getMessage(), errorId);
		}
		
		if (currentUser != null) {
			cart = currentUser.getShoppingCart();
		}
		
		return cart;
	}
	
	private void cleanShoppingCart() {
		CurrentUser currentUser = null;
		
		try {
			currentUser = (CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch(Throwable errorId) {
			currentUser = null;
			log.error(errorId.getMessage(), errorId);
		}
		
		if (currentUser != null) {
			currentUser.setShoppingCart(null);
		}
	}
	
	@Override
	public ShoppingCart addProductToShoppingCart(Long productId, Integer qtd, boolean hasAdd) {
		
		Product product = this.find(productId);
		
		ShoppingCart cart = this.getShoppingCart();
		
		if (product != null && cart != null) {
			
			List<ProductInCart> selectedProducts = cart.getProducts();
			
			ProductInCart prod = null;
			
			boolean hasInsert = true;
			
			if (selectedProducts == null) {
				selectedProducts = new ArrayList<ProductInCart>();
			} else {
				for (int i = 0; i < selectedProducts.size(); i++) {
					if (selectedProducts.get(i) != null && selectedProducts.get(i).getProduct().getId().equals(product.getId())) {
						prod = selectedProducts.get(i);
						
						if (hasAdd) {
							prod.setQtdSelected(prod.getQtdSelected() + qtd);	
						} else {
							prod.setQtdSelected(qtd);
						}
						
						
						if (prod.getQtdSelected() > product.getQtdAvailable()) {
							prod.setQtdSelected(product.getQtdAvailable());
						}
						
						hasInsert = false;
					}
				}
			}
			
			if (prod == null) {
				prod = new ProductInCart();
				
				prod.setShoppingCart(cart);
				prod.setProduct(product);
				prod.setQtdSelected(qtd);
			}
			
			if (hasInsert) {
				selectedProducts.add(prod);	
			}
			
			cart.setProducts(selectedProducts);
			
		}
		
		return cart;
	}

	@Override
	@Transactional
	public ShoppingCart checkout() {
		ShoppingCart cart = getShoppingCart();
		
		cart.setUser(getAuthenticatedUser());
		
		cart.setCreatedBy(getAuthenticatedUser());
		cart.setCreationDate(new Date());
		
		if (cart != null && cart.getProducts() != null && !cart.getProducts().isEmpty()) {
			for (ProductInCart prod : cart.getProducts()) {
				prod.setCreatedBy(getAuthenticatedUser());
				prod.setCreationDate(new Date());
				
				prod.getProduct().setQtdAvailable(prod.getProduct().getQtdAvailable() - prod.getQtdSelected());
				
				productRepository.save(prod.getProduct());
			}
			
			shoppingCartRepositpry.save(cart);
			
			cleanShoppingCart();
		}

		return cart;
	}

	@Override
	public ShoppingCart removeProductFromShoppingCart(Long productId) {
		Product product = this.find(productId);
		
		ShoppingCart cart = this.getShoppingCart();
		
		if (product != null && cart != null) {
			
			List<ProductInCart> selectedProducts = cart.getProducts();
			
			ProductInCart prod = null;
			
			boolean canRemove = false;
			
			if (selectedProducts == null) {
				selectedProducts = new ArrayList<ProductInCart>();
			} else {
				for (int i = 0; i < selectedProducts.size(); i++) {
					if (selectedProducts.get(i) != null && selectedProducts.get(i).getProduct().getId().equals(product.getId())) {
						prod = selectedProducts.get(i);
						
						canRemove = true;
					}
				}
			}
			
			if (canRemove) {
				selectedProducts.remove(prod);	
			}
			
			cart.setProducts(selectedProducts);
		}
		
		return cart;
	}
}
