package br.apolo.business.service;

import org.springframework.data.domain.Page;

import br.apolo.business.model.FileContent;
import br.apolo.data.model.Product;
import br.apolo.data.model.ShoppingCart;

public interface ProductService extends BaseService<Product> {

	final int PAGE_SIZE = 50;
	
	ShoppingCart getShoppingCart();
	
	ShoppingCart addProductToShoppingCart(Long productId, Integer qtd, boolean hasAdd);
	
	ShoppingCart removeProductFromShoppingCart(Long productId);
	
	ShoppingCart checkout();

	/**
	 * List all products available to shop (qtd greater than zero)
	 */
	Page<Product> list(Integer pageNumber);
	
	
	/**
	 * Save Product
	 * @param product
	 * @param file
	 * @return Product
	 */
	Product save(Product product, FileContent file);
	
	/**
	 * Search product
	 * @param pageNumber
	 * @param param
	 * @return Page<Product>
	 */
	Page<Product> search(Integer pageNumber, String param);
	
}