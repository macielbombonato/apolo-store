<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<jsp:include page='/WEB-INF/views/fragment/_loadingDialog.jsp'></jsp:include>
<jsp:include page='/WEB-INF/views/fragment/_editDataDialog.jsp'></jsp:include>
<jsp:include page='/WEB-INF/views/fragment/_removeConfirmationDialog.jsp'></jsp:include>
<jsp:include page='/WEB-INF/views/fragment/_javascript.jsp'></jsp:include>
