<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="icon" type="image/png" href='<c:url value="/resources/app/img/favicon.png" />'>

<link href='<c:url value="/resources/plugin/bootstrap/css/bootstrap.css" />' rel="stylesheet" media="screen"/>
<link href='<c:url value="/resources/plugin/bootstrap/css/bootstrap-theme.css" />' rel="stylesheet" />

<link href='<c:url value="/resources/plugin/jquery/themes/base/jquery-ui.css" />' rel="stylesheet" />

<link href='<c:url value="/resources/plugin/jquery-chosen/chosen.css" />' rel='stylesheet'>

<link href='<c:url value="/resources/plugin/tagit/css/jquery.tagit.css" />' rel='stylesheet'>
<link href='<c:url value="/resources/plugin/tagit/css/tagit.ui-zendesk.css" />' rel='stylesheet'>

<link href='<c:url value="/resources/plugin/cleditor/jquery.cleditor.css" />' rel='stylesheet'>

<link href='<c:url value="/resources/plugin/mediaelement/mediaelementplayer.min.css" />' rel='stylesheet'>

<link href='<c:url value="/resources/app/css/app.css" />' rel='stylesheet'>