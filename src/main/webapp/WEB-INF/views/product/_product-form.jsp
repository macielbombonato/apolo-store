<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="apolo" uri="/WEB-INF/taglib/apolo.tld" %>

<s:message code="common.datePattern" var="datePattern" />

<jsp:useBean id="inputlength" class="br.apolo.common.util.InputLength"/>

<input type="hidden" id="id" name="id" value="${product.id}" />
<input type="hidden" id="pictureOriginalName" name="pictureOriginalName" value="${product.pictureOriginalName}" />
<input type="hidden" id="pictureGeneratedName" name="pictureGeneratedName" value="${product.pictureGeneratedName}" />

<ul class="nav nav-tabs" id="formTab">
	<li class="active">
		<a href="#home">
			<span class="glyphicon glyphicon-tasks"></span>
		</a>
	</li>
	<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_CREATE, ROLE_PRODUCT_EDIT">
		<c:if test="${readOnly}">
			<li>
				<a href="#authorShipTab">
					<span class="glyphicon glyphicon-user"></span>
				</a>
			</li>
		</c:if>
	</security:authorize>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="home">
		<div class="form-group">
			<label for="name" class="control-label">
				<s:message code="product.name" />
			</label>
			<input type="text" id="name" name="name" class="form-control" value="${product.name}" <c:if test="${readOnly}">readonly="true"</c:if> />
		</div>
		
		<div class="form-group">
			<label for="qtdAvailable" class="control-label">
				<s:message code="product.qtdAvailable" />
			</label>
			<input type="text" id="qtdAvailable" name="qtdAvailable" class="form-control" value="${product.qtdAvailable}" <c:if test="${readOnly}">readonly="true"</c:if> />
		</div>
		
		<div class="form-group">
			<label for="price" class="control-label">
				<s:message code="product.price" />
			</label>
			<input type="text" id="price" name="price" class="form-control" value="${product.price}" <c:if test="${readOnly}">readonly="true"</c:if> />
		</div>
		
		<div class="form-group">
			<label for="description" class="control-label">
				<s:message code="product.description" />
			</label>
			<textarea id="description" name="description" rows="5" cols="80" class="form-control" <c:if test="${readOnly}">readonly="true"</c:if>
				>${product.description}</textarea>
		</div>
		
		<c:if test="${!readOnly}">
			<div class="form-group">
				<label for="picturefiles" class="control-label">
					<s:message code="product.picturefiles" />
				</label>
				<input type="file" class="form-control" name="picturefiles[0]" />
			</div>		
		</c:if>
		
	</div>
	
	<div class="tab-pane" id="authorShipTab">
		<jsp:include page='_authorship.jsp'></jsp:include>
	</div>
</div>
