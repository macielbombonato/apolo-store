<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
	<jsp:include page='/WEB-INF/views/fragment/_pageheader.jsp'></jsp:include>
	<body>
		<jsp:include page='/WEB-INF/views/fragment/_contentheader.jsp'></jsp:include>
		
		<div class="container ">
			<div class="col-sm-8">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<strong>
							<s:message code="product.view.title" />
						</strong>
					</div>
					<div class="panel-body">
						<div class="col-sm-4">
							<center>
								<c:choose>
									<c:when test="${product.pictureGeneratedName != null && not empty product.pictureGeneratedName}">
										<img class="img-thumbnail img-responsive" src="<s:url value="/uploadedfiles/Product"></s:url>/${product.id}/${product.pictureGeneratedName}" />
									</c:when>
									<c:otherwise>
										<h1>
											<span class="glyphicon glyphicon-user"> </span>
										</h1>
									</c:otherwise>
								</c:choose>
							</center>
						</div>
						
						<div class="col-sm-8">
							<jsp:include page='_product-form.jsp'></jsp:include>
							
							<div class="form-actions">
								<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_EDIT">
									<a href='<s:url value="/product/edit"></s:url>/${product.id}' class="btn btn-default">
										<i class="glyphicon glyphicon-edit"></i>
										<s:message code="common.edit" />
									</a>
								</security:authorize>
								
								<a href='#' class="btn btn-default back" > 
									<i class="glyphicon glyphicon-backward"></i>
									<s:message code="common.back" />
								</a>
								
								<a href='#' class="btn btn-default" 
										onclick="callAddProductToShoppingCart('<s:url value="/product/addProductToShoppingCart"></s:url>/true/${product.id}/1', '${product.id}', '${product.name}', '${product.price}', '1');" data-toggle="tooltip" title="<s:message code="shopping.cart.add.tip" />">
									<i class="glyphicon glyphicon-shopping-cart"></i>
									<s:message code="shopping.cart.add" />
								</a>
							</div>		
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<jsp:include page='/WEB-INF/views/product/_summarized_cart.jsp'></jsp:include>
			</div>
		</div>
		
		<jsp:include page='/WEB-INF/views/fragment/_contentfooter.jsp'></jsp:include>
		<jsp:include page='/WEB-INF/views/fragment/_pagefooter.jsp'></jsp:include>
	</body>
</html>