<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
	<jsp:include page='/WEB-INF/views/fragment/_pageheader.jsp'></jsp:include>
	<body>
		<jsp:include page='/WEB-INF/views/fragment/_contentheader.jsp'></jsp:include>
		
		<div class="container ">
			<div class="row ">

				<jsp:include page='_search-form.jsp'></jsp:include>
				
				<br />
				
				<div class="panel panel-primary">
					<div class="panel-heading">
						<strong>
							<s:message code="product.list.title" />
						</strong>
					</div>
					<center>
						<jsp:include page='/WEB-INF/views/fragment/_pagination.jsp'></jsp:include>
					</center>
					<c:choose>
						<c:when test="${productList != null && not empty productList}">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>
												<s:message code="product.picturefiles" />
											</th>
											<th>
												<s:message code="product.name" />
											</th>
											<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_EDIT">
												<th>
													<s:message code="product.qtdAvailable" />
												</th>
											</security:authorize>
											<th>
												<s:message code="product.price" />
											</th>
											<th>
												<s:message code="common.actions" />
											</th>
										</tr>
									</thead>
									
									<tbody>
										<c:forEach items="${productList}" var="product">
											<tr id="product_${product.id}">
												<td>
													<center>
														<a href='<s:url value="/product/view"></s:url>/${product.id}' class="btn btn-link">
															<c:choose>
																<c:when test="${product.pictureGeneratedName != null && not empty product.pictureGeneratedName}">
																	<img class="img-thumbnail img-responsive" src="<s:url value="/uploadedfiles/Product"></s:url>/${product.id}/${product.pictureGeneratedName}" style="width: 150px;"/>
																</c:when>
																<c:otherwise>
																	<span class="glyphicon glyphicon-user"> </span>
																</c:otherwise>
															</c:choose>
														</a>								
													</center>
												</td>
												<td>
													<a href='<s:url value="/product/view"></s:url>/${product.id}' class="btn btn-link">
														${product.name}
													</a>
												</td>
												<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_EDIT">
													<td>
														${product.qtdAvailable}
													</td>
												</security:authorize>
												<td>
													${product.price}
												</td>
												<td>
													<div class="btn-group btn-group-sm">
														<a href='<s:url value="/product/view"></s:url>/${product.id}' class="btn btn-default btn-small" data-toggle="tooltip" title="<s:message code="common.show" />">
															<i class="glyphicon glyphicon-zoom-in"></i>
														</a>
														<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_EDIT">
															<a href='<s:url value="/product/edit"></s:url>/${product.id}' class="btn btn-default btn-small" data-toggle="tooltip" title="<s:message code="common.edit" />">
																<i class="glyphicon glyphicon-edit"></i>
															</a>
														</security:authorize>
														<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_REMOVE">
															<a href='#' class="btn btn-default btn-small" onclick="removeConfirmationDialogOpen('<s:url value="/product/remove"></s:url>/${product.id}', 'product_${product.id}');" data-toggle="tooltip" title="<s:message code="common.remove" />">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														</security:authorize>
													</div>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:when>
						<c:otherwise>
							<div class="panel-body">
								<p>
									<s:message code="common.nodatafound" htmlEscape="false"/>
								</p>
							</div>
						</c:otherwise>
					</c:choose>
						<center>
							<jsp:include page='/WEB-INF/views/fragment/_pagination.jsp'></jsp:include>
						</center>
				</div>
			</div>
		</div>
		
		<jsp:include page='/WEB-INF/views/fragment/_contentfooter.jsp'></jsp:include>
		<jsp:include page='/WEB-INF/views/fragment/_pagefooter.jsp'></jsp:include>
	</body>
</html>