<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
	<jsp:include page='/WEB-INF/views/fragment/_pageheader.jsp'></jsp:include>
	<body>
		<jsp:include page='/WEB-INF/views/fragment/_contentheader.jsp'></jsp:include>
		
		<div class="container ">
			<div class="row ">

				<div class="panel panel-primary">
					<div class="panel-heading">
						<strong>
							<s:message code="product.showCart.title" />
						</strong>
					</div>
					<c:choose>
						<c:when test="${shoppingCart != null && not empty shoppingCart}">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>
												<s:message code="product.picturefiles" />
											</th>
											<th>
												<s:message code="product.name" />
											</th>
											<th>
												<s:message code="product.qtdAvailable" />
											</th>
											<th>
												<s:message code="shopping.cart.qtd.selected" />
											</th>
											<th>
												<s:message code="product.price" />
											</th>
											<th>
												Total do produto
											</th>
											<th>
												<s:message code="common.actions" />
											</th>
										</tr>
									</thead>
									
									<tbody>
										<c:set var="totalPrice" value="0" />
										<c:forEach items="${shoppingCart.products}" var="prod">
											<c:set var="totalPrice" value="${totalPrice + (prod.product.price * prod.qtdSelected)}" />
											<tr id="product_${prod.product.id}">
												<td>
													<center>
														<a href='<s:url value="/product/view"></s:url>/${prod.product.id}' class="btn btn-link">
															<c:choose>
																<c:when test="${prod.product.pictureGeneratedName != null && not empty prod.product.pictureGeneratedName}">
																	<img class="img-thumbnail img-responsive" src="<s:url value="/uploadedfiles/Product"></s:url>/${prod.product.id}/${prod.product.pictureGeneratedName}" style="width: 150px;"/>
																</c:when>
																<c:otherwise>
																	<span class="glyphicon glyphicon-user"> </span>
																</c:otherwise>
															</c:choose>
														</a>								
													</center>
												</td>
												<td>
													<a href='<s:url value="/product/view"></s:url>/${prod.product.id}' class="btn btn-link">
														${prod.product.name}
													</a>
												</td>
												<td>
													<span id="qtdAvailable">${prod.product.qtdAvailable}</span>
												</td>
												<td>
													<input type="text" 
															id="qtdSelected" 
															name="qtdSelected" 
															class="form-control" 
															value="${prod.qtdSelected}" 
															onchange="recalculateCart('<s:url value="/product/addProductToShoppingCart"></s:url>/false/${prod.product.id}/', '${prod.product.id}', '${prod.product.name}', '${prod.product.price}')"
														/>
												</td>
												<td>
													<span id="price">${prod.product.price}</span>
												</td>
												<td>
													<span id="productTotalPrice">${prod.product.price * prod.qtdSelected}</span>
												</td>
												<td>
													<div class="btn-group btn-group-sm">
														<a href='<s:url value="/product/view"></s:url>/${prod.product.id}' class="btn btn-default btn-small" data-toggle="tooltip" title="<s:message code="common.show" />">
															<i class="glyphicon glyphicon-zoom-in"></i>
														</a>
														<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_REMOVE">
															<a href='#' class="btn btn-default btn-small" onclick="removeProductFromCart('<s:url value="/product/remove-from-cart"></s:url>/${prod.product.id}', 'product_${prod.product.id}', '${prod.product.id}');" data-toggle="tooltip" title="<s:message code="common.remove" />">
																<i class="glyphicon glyphicon-remove"></i>
															</a>
														</security:authorize>
													</div>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								
								<strong>
									<s:message code="shopping.cart.total.price" />: <span id="totalPrice"><c:out value="${totalPrice}" /> </span>
								</strong>
								<br />
								<a href='<s:url value="/product/checkout"></s:url>' class="btn btn-default">
									<i class="glyphicon glyphicon-shopping-cart"></i>
									<s:message code="shopping.cart.checkout" />
								</a>
							</div>
						</c:when>
						<c:otherwise>
							<div class="panel-body">
								<p>
									<s:message code="common.nodatafound" htmlEscape="false"/>
								</p>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		
		<jsp:include page='/WEB-INF/views/fragment/_contentfooter.jsp'></jsp:include>
		<jsp:include page='/WEB-INF/views/fragment/_pagefooter.jsp'></jsp:include>
	</body>
</html>