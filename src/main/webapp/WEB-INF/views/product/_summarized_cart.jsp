<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="apolo" uri="/WEB-INF/taglib/apolo.tld" %>

<s:message code="common.datePattern" var="datePattern" />

<div class="panel panel-primary">
	<div class="panel-heading">
		<strong>
			<s:message code="shopping.cart" />
		</strong>
	</div>
	<div class="panel-body">
		<div class="col-sm-12">

			<security:authorize access="isAuthenticated()">
				<security:authentication property="principal.shoppingCart" var="cart" />
					<c:choose>
						<c:when test="${cart != null}">
							<c:set var="totalPrice" value="0" />
							
							<div id="products">
								<c:forEach items="${cart.products}" var="prod">
									<div class="alert alert-success" id="productCart_${prod.product.id}">
										<ul class="list-group">
											<li class="list-group-item">${prod.product.name}</li>
											<li class="list-group-item">${prod.product.price}</li>
											<li class="list-group-item" id="productQtd" >${prod.qtdSelected}</li>
											<li class="list-group-item" id="productTotalPrice" >${prod.product.price * prod.qtdSelected}</li>
										</ul>
									</div>
									<c:set var="totalPrice" value="${totalPrice + (prod.product.price * prod.qtdSelected)}" />
								</c:forEach>
							</div>
							
							<ul class="list-group">
								<li class="list-group-item">
									<strong>
										<s:message code="shopping.cart.total.price" />: <span id="totalPrice"><c:out value="${totalPrice}" /> </span>
									</strong>
								</li>
							</ul>
							
							<br />
							<a href='<s:url value="/product/showCart"></s:url>' class="btn btn-default">
								<i class="glyphicon glyphicon-shopping-cart"></i>
								<s:message code="shopping.cart.showCart" />
							</a>
						</c:when>
						<c:otherwise>
							<s:message code="shopping.cart.empty" />
						</c:otherwise>
					</c:choose>
			</security:authorize>

			<security:authorize access="!isAuthenticated()">
				<s:message code="shopping.cart.not.logged" />
			</security:authorize>

		</div>
	</div>
</div>
