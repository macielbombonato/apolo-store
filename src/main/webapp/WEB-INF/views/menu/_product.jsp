<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<ul class="nav navbar-nav">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="glyphicon glyphicon-cog "></i>
			<span class="hidden-phone">
				<s:message code="product.menu.header" />
			</span>
			<span class="caret"></span>
		</a>
                       
		<ul class="dropdown-menu">
			<li class="dropdown-header">
				<s:message code="products" />
			</li>
			<li>
				<a href='<s:url value="/product/list"></s:url>'>
					<i class="glyphicon glyphicon-th-list"></i>
					<s:message code="product.list" />
				</a>
			</li>
			<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_CREATE, ROLE_PRODUCT_MANAGER">
				<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_PRODUCT_CREATE">
					<li>
						<a href='<s:url value="/product/new"></s:url>'>
							<i class="glyphicon glyphicon-plus"></i>
							<s:message code="product.new" />
						</a>
					</li>
				</security:authorize>
			</security:authorize>
		</ul>
	</li>           
</ul>