$(document).ready(function() {
	$('#showPassword').change(function(){
		var passwordText = $('#password');
		if($(this).attr('checked') == 'checked'){
			passwordText.get(0).type = 'text';
		} else {
			passwordText.get(0).type = 'password';
		}
		
		var passwordConfirmationText = $('#passwordConfirmation');
		if($(this).attr('checked') == 'checked'){
			passwordConfirmationText.get(0).type = 'text';
		} else {
			passwordConfirmationText.get(0).type = 'password';
		}
	});
	
	$('button[type=submit]').click(function(e) {
		$('#loadingDialog').modal('toggle');
	});
	
	$('#loadingDialog').modal({
		backdrop: 'static',
		keyboard: false,
		show: false
	});
	
    $('a.back').click(function(){
        parent.history.back();
        return false;
    });
	
	$('#editData').modal({
		backdrop: 'static',
		keyboard: false,
		show: false
	});
	
	$( ".btn" ).tooltip({
		'selector': '',
		'placement': 'top'
	});
	
	// Tooltip
	$( ".tooltipLeft" ).tooltip({
		'selector': '',
		'placement': 'left'
	});
	
	$( ".tooltipRight" ).tooltip({
		'selector': '',
		'placement': 'right'
	});
	
	$( ".tooltipBottom" ).tooltip({
		'selector': '',
		'placement': 'bottom'
	});
	
	$( ".tooltipTop" ).tooltip({
		'selector': '',
		'placement': 'top'
	});
	
	// Popover
	$(".applyPopover").popover();
	
	$(".applyPopoverHover").popover({
		'trigger': 'hover' 
	});
	
	// set the defaults for the datepicker
	jQuery(function($){
		$.datepicker.regional['pt-BR'] = {
		closeText: 'Fechar',
		prevText: '&#x3c;Anterior',
		nextText: 'Pr&oacute;ximo&#x3e;',
		currentText: 'Hoje',
		monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
		$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	});
	
	$('.applyDatePicker').each(function(){
		$(this).datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
	
	$(".applyTagit").tagit({
		allowSpaces : true, 
		readOnly : App.readOnly
	});
	
	$('.tagit').each(function(){
		$(this).css('margin-left', '0px');
	});
	
	$(".applyChosen").chosen({
		allow_single_deselect:true
	});
	
	$(".applyCleditor").cleditor({
		width: '99%',
		height: '80%',
		disabled: App.readOnly
	});
	
	$(".ellipsis").ellipsis({
		row: 10, 
		char: '...'
	});
	
	$('#formTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
});

$('#header').height($('#menu').height() + 5);

$('video').mediaelementplayer({
	success: function(media, node, player) {
		$('#' + node.id + '-mode').html('mode: ' + media.pluginType);
	}
});

if (!App.readOnly) {
	$('.focus').trigger('focus');
}

function editDataOpen(fieldName) {
	$('#dataInput').val($('#'+fieldName).val());
	$('#editingFieldName').val(fieldName);
	$('#editData').modal('toggle');
} 

function editData() {
	$('#'+$('#editingFieldName').val()).val($('#dataInput').val());
	$('#label_'+$('#editingFieldName').val()).text($('#dataInput').val());
	$('#dataInput').val('');
	$('#editingFieldName').val('');
	$('#editData').modal('toggle');
}

function removeConfirmationDialogOpen(url, objectId) {
	$('#removeConfirmationObjectId').val(objectId);
	$('#removeConfirmationUrl').val(url);
	$('#removeConfirmationDialog').modal('toggle');
} 

function removeRedirectionConfirmationDialogOpen(url, objectId) {
	$('#removeRedirectionConfirmationObjectId').val(objectId);
	$('#removeRedirectionConfirmationUrl').val(url);
	$('#removeRedirectionConfirmationDialog').modal('toggle');
	
	$('#btnCallRemove').attr('href', $('#removeRedirectionConfirmationUrl').val());
}

function callRemove() {
	$.ajax({
		type : "GET",
		url : $('#removeConfirmationUrl').val(),
		beforeSend : function(xhr) {
			$('#loadingDialog').modal('toggle');
		},
		success : function(response) {
			var json = $.parseJSON(response);
			
			$('#removeMsg').text(json.result.message);
			
			if (json.result.success) {
				$('#'+$('#removeConfirmationObjectId').val()).remove();		
			}
			
			$('#loadingDialog').modal('toggle');
			$('#removeConfirmationDialog').modal('toggle');
			$('#removeMsgDialog').modal('toggle');
		},
		complete : function() {
			$('#removeConfirmationObjectId').val('');
			$('#removeConfirmationUrl').val('');
		}
	});
}

function callAddProductToShoppingCart(url, id, name, price, qtd) {
	$.ajax({
		type : "GET",
		url : url,
		beforeSend : function(xhr) {
			$('#loadingDialog').modal('toggle');
		},
		success : function(response) {
			var json = $.parseJSON(response);
			
			$('#products').html('');
			
			for(i=0;i<json.result.cart.products.length;i++) {
				prod = json.result.cart.products[i];
				
				$('#products').append(
					'<div class="alert alert-success" id="productCart_'+prod.id+'">'+
						'<ul class="list-group">'+
							'<li class="list-group-item">'+ prod.name +'</li>'+
							'<li class="list-group-item">'+ prod.price +'</li>'+
							'<li class="list-group-item" id="productQtd" >'+ prod.qtd +'</li>'+
							'<li class="list-group-item" id="productTotalPrice" >'+ prod.productTotalPrice +'</li>'+
						'</ul>'+
					'</div>'
				);
			}
			
			$('#totalPrice').text(json.result.cart.totalPrice);
			
			$('#loadingDialog').modal('toggle');
		},
		complete : function() {
		}
	});
}

function recalculateCart(url, id, name, price) {
	qtdAvailable = $('#product_'+id).find('#qtdAvailable').text();
	qtdSelected = $('#product_'+id).find('#qtdSelected').val();
	price = $('#product_'+id).find('#price').text();
	productTotalPrice = $('#product_'+id).find('#productTotalPrice').text();
	totalPrice = $('#totalPrice').text();
	
	if (parseInt(qtdAvailable) < parseInt(qtdSelected)) {
		qtdSelected = qtdAvailable;
		$('#product_'+id).find('#qtdSelected').val(qtdAvailable);
	}
	
	totalPrice = parseFloat(totalPrice) - parseFloat(productTotalPrice);
	
	productTotalPrice = parseFloat(price) * parseInt(qtdSelected);
	$('#product_'+id).find('#productTotalPrice').text(productTotalPrice);
	
	totalPrice = parseFloat(totalPrice) + parseFloat(productTotalPrice);
	
	$('#totalPrice').text(totalPrice);
	
	callAddProductToShoppingCart(url + qtdSelected, id, name, price, qtdSelected);
}

function removeProductFromCart(url, lineId, id) {
	productTotalPrice = $('#product_'+id).find('#productTotalPrice').text();
	totalPrice = $('#totalPrice').text();
	
	totalPrice = parseFloat(totalPrice) - parseFloat(productTotalPrice);
	
	$('#totalPrice').text(totalPrice);
	
	removeConfirmationDialogOpen(url, lineId);
}