# Apolo-Store
============

Este projeto é apenas um modelo de loja viritual com alguns componentes para provar um conceito, questões de pagamento, envio de email e demais funcionalidadas não foram implementadas (ainda ;-) ).

# Arquitetura Apolo

Este projeto é uma iniciativa open source para que os desenvolvedores ganhem tempo no startup de seus sistemas, tendo uma base consistente e de fácil expansão.

## Faça sua cópia

O apolo é versionado com git, portanto, para fazer sua cópia do código e controlá-la, será necessário ter o git em sua máquina.

Há algum tempo eu fiz um "tutorialzinho" de alguns comandos git e para que eles servem: [http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html](http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html), Este tutorial deve ajudar um pouco para realizar operações via linha de comando.

## Crie seu Fork deste projeto

Caso queira montar um projeto a partir deste e queira deixá-lo versionado aqui no bitbucket, use a opção "Fork" que encontra-se no topo da tela. O processo é rápido e o passo a passo é bem simples.

Espero que este projeto lhe seja útil e tendo sugestões ou encontrando problemas, por favor, entre em contato ou abra uma issue aqui no bitbucket.

# Licença
---------

Apolo-Base é um software de código aberto, você pode redistribuí-lo e/ou
modificá-lo conforme a licença Apache versão 2.0. Veja o arquivo LICENSE-Apache.txt
